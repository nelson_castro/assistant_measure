# -*- coding: utf-8 -*-
"""
Created on Mon May  1 11:22:01 2023

@author: nelso
"""

# Import essential libraries
import warnings
warnings.filterwarnings("ignore")

import cv2
import numpy as np

from segment_anything import sam_model_registry, SamPredictor

import src.constants as cst
from src.sam_mask_compute import extract_sam_mask
from src.get_cropped_size import get_contours_metrics_live


def click_and_crop(event,x,y,flags,params):
    global counter, point_matrix
    # Left button mouse click event opencv
    if event == cv2.EVENT_LBUTTONDOWN:
        point_matrix[counter] = x, y
        counter = counter + 1

            
def main(cap, point_matrix, cut_to_request=True, get_my_dims=False):
    
    '''
    This function uses the IP Webcam output. By default, it's just filming.
    
    After hitting left button (Mouse) twice in two different area of the image and "SPACE BAR" button,
    a screenshot is taken and Segment Anything Model is run upon this screenshot to extract
    two objects (each object you want to point by a left button click) masks (here ~contours).
    
    With theses masks and a reference size, this functions computes each object size 
    (using the extreme-left object as reference) and displays the dimensions of each of
    them continuously until the "s" button is hit. This "s" button frees the left button click
    coordinates and let you start over.
    
    Press "c" to quit.
    
    Parameters:
            cap (array) : Streaming video, obtained using IP Webcam
            point_matrix (array) : Contains cv2.EVENT_LBUTTONDOWN click coordinates
            cut_to_request (Bool) : By default True when the user want to measure
            the target object, it's converted to False once this measure is done.
            get_my_dims (Bool) : By default False when the target object is not 
            measured yet, it's converted to True inside the While loop.
    '''    
    
    
    global counter
    
    # While loop to continuously fetching data from the Url
    while True:
        ret, frame = cap.read()
        
        if not ret:
            cap = cv2.VideoCapture(cst.URL)
            continue
        
        cv2.namedWindow("Android_cam")
        cv2.setMouseCallback("Android_cam", click_and_crop)
        
        key = cv2.waitKey(1) & 0xFF
    
        if key == ord("c"):
            break
        
        if key == ord("s"):
            counter = 0
            get_my_dims = False
            cut_to_request = True
        
        if (key % 256 == 32) & (counter==2):
            # SPACE pressed
            #cv2.imwrite("my_mask.png", masks_bw)     
            print("CHARGEMENT DE L'IMAGE DANS SAM.")
            #masks_bw, get_my_dims = extract_sam_mask(predictor, point_matrix, frame)
            masks_bw, get_my_dims = cv2.imread("in_memory_to_disk.png")[:,:,1], True 
            print("MASQUE OBTENU.")
    
        if get_my_dims:
            get_contours_metrics_live(frame, masks_bw, cut_to_request, pixelsPerMetric = None)
            cut_to_request = False
        
        cv2.imshow("Android_cam", frame)
    
    # When everything done, release the capture
    cap.release()
    cv2.destroyAllWindows()


if __name__ == "__main__":

    print("CHARGEMENT DE SAM.")
    sam = sam_model_registry[cst.MODEL_TYPE](checkpoint=cst.SAM_CHECKPOINT)
    sam.to(device=cst.DEVICE)
    predictor = SamPredictor(sam)

    # Replace the below URL with your own. Make sure to add "/shot.jpg" at last.
    print("CAPTURE DU FLUX DE WEBCAM.")
    cap = cv2.VideoCapture(cst.URL) #record webcam
    
    point_matrix = np.zeros((2,2), int)
    counter=0
     
    main(cap, point_matrix)