# Description

This repository is a Python program (not packaged yet) that helps you measure an object using IP Webcam. Once this object is measured, it can then be "resized" (precisely, it could be seen as resizing will appear as cut marks) thanks to a vocal feature the user can launch in order to declare the new dimensions he wants for the object.

## Installation

Make sure to have [IPWebcam] (https://play.google.com/store/apps/details?id=com.pas.webcam&hl=fr&gl=US) installed on your phone.

Then, install the environment with the following commands:
```bash
# In your working space
git clone https://gitlab.com/nelson_castro/assistant_measure.git

# Create sub-directory
cd measure_assistant

mkdir weights

# Initialize Venv
python -m venv venv

# Activate Venv
venv\Scripts\activate

# Install packages
pip install -r requirements.txt

# Install SAM from META
pip install git+https://github.com/facebookresearch/segment-anything.git
```

### Run rules

* Make sure your Phone Camera is stable, it must be fixed.
* Make sure you have two object in the image : A reference object (you know it's size) and a target object (you want to know its size).
* The reference object should always be on the left part of the image.
* The reference object should be circular, with this we are sure to be able to measure it using only one information (the diameter) instead of two (width and height).

Moreover, a few parameters must be set before running the script:

```python
# SAM INFORMATION
MODEL_TYPE = "vit_h"

SAM_CHECKPOINT = 'weights/sam_vit_h_4b8939.pth'

DEVICE = "cpu"

# URL TO YOUR IP WEBCAM
URL = "http://192.168.1.17:8080/video"

# OBJECT REFERENCE DIAMETER in CM (It expects a circular object)
REF_SIZE = 2.325

# LANGUAGE FOR SPEECH TO TEXT
LANGUAGE = "fr-FR"
```

## Usage

Before running the main script, you need to launch a IP Webcam server on your phone (with Wifi activated), if you don't, script will raise an error. 

Once this and the run rules are guaranteed, just run the following command :
```bash
python main.py
```

## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.