# -*- coding: utf-8 -*-
"""
Created on Tue May 23 21:52:56 2023

@author: nelso
"""

# SAM INFORMATION
MODEL_TYPE = "vit_h"

SAM_CHECKPOINT = 'weights/sam_vit_h_4b8939.pth'

DEVICE = "cpu"

# URL TO YOUR IP WEBCAM
URL = "http://192.168.1.17:8080/video"

# OBJECT REFERENCE DIAMETER in CM (It expects a circular object)
REF_SIZE = 2.325

# LANGUAGE FOR SPEECH TO TEXT
LANGUAGE = "fr-FR"