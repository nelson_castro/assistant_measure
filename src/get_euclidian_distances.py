# -*- coding: utf-8 -*-
"""
Created on Tue May 23 21:42:36 2023

@author: nelso
"""

import cv2
import imutils
from imutils import perspective
import numpy as np

from scipy.spatial import distance as dist

import src.constants as cst

def get_the_box(contour):

    '''
    Search for a "oui" substring in a string, needed for future object resizing. 
    
    Parameters:
            text_from_speech (str): Output of speechrecognition library

    Returns:
            Boolean, True if "oui" in text_from_speech else False
    '''        

    box = cv2.minAreaRect(contour)
    box = cv2.cv.BoxPoints(box) if imutils.is_cv2() else cv2.boxPoints(box)
    box = np.array(box, dtype="int")
    
    return perspective.order_points(box)

def midpoint(ptA, ptB):    
    
    '''
    Takes two point in a image and find the point (middle point) cutting the
    distance between these two points in two equal sized segments.
    
    Parameters:
            ptA (array): coordinates of a point 
            ptB (array): coordinates of a point 
    
    Returns:
            (tuple) : Contains intermediary point (middle point)
                              between two points.
    '''     
    
    return ((ptA[0] + ptB[0]) * 0.5, (ptA[1] + ptB[1]) * 0.5)

def get_box_mid_points(box):
	
    '''
    This function runs midpoint() on each rectangle side of the box parameter (output of get_the_box()).
    
    Parameters:
            box (array) : Coordinates of the rectangle we need to measure
    
    Returns:
            mid_dict (dict) : Contains intermediary point (middle point)
                              of each rectangle line (build from box input)
    '''        
    
    (tl, tr, br, bl) = box
    
    (tltrX, tltrY) = midpoint(tl, tr)
    (blbrX, blbrY) = midpoint(bl, br)
    (tlblX, tlblY) = midpoint(tl, bl)
    (trbrX, trbrY) = midpoint(tr, br)
    
    return {"up_mid" : (int(tltrX), int(tltrY)),
          "bottom_mid" : (int(blbrX), int(blbrY)),
          "left_mid" : (int(tlblX), int(tlblY)),
          "right_mid" : (int(trbrX), int(trbrY))}


def compute_euclidian_distances(mid_dict, pixelsPerMetric, ref_size = cst.REF_SIZE):

    '''
    This function uses rectangle middle points and a reference size of an object visible
    in the image (should be the extreme-left object in the image and a circle) 
    to extract pixelsPerMetric in order to measure every object in the image.
    
    Parameters:
            mid_dict (dict) : Contains intermediary point (middle point)
                              of each rectangle line
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
            ref_size (float) : size of the reference object in centimeter
                               or other metric
    
    Returns:
            (tuple) : 2 float elements, contains dimensions of an object
                                 (width in cm, height in cm) 
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
    '''         


    # compute the Euclidean distance between the midpoints
    dA = dist.euclidean(mid_dict["up_mid"], mid_dict["bottom_mid"])
    dB = dist.euclidean(mid_dict["left_mid"], mid_dict["right_mid"])
    # if the pixels per metric has not been initialized, then
    # compute it as the ratio of pixels to supplied metric
    # (in this case, cm)
    if pixelsPerMetric is None:
        pixelsPerMetric = dB / ref_size
    
    width_size = dB / pixelsPerMetric
    height_size = dA / pixelsPerMetric
      
    return (width_size, height_size), pixelsPerMetric

def compute_needed_metrics(contour, pixelsPerMetric):

    '''
    This function converts contours into rectangle that we can measure the width and
    the height from. Even non-squared object are turned into rectangle.
    
    Parameters:
            contour (array) : it contains the coordinates representing the contours of an object.
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
    
    Returns:
            box (array) : Coordinates of the rectangle we need to measure 
            mid_dict (dict) : Contains intermediary point (middle point)
                              of each rectangle line (build from box input)
            object_dims (tuple) : 2 float elements, contains dimensions of an object
                                 (width in cm, height in cm) 
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
    '''        

    box = get_the_box(contour)
    mid_dict = get_box_mid_points(box)
    object_dims, pixelsPerMetric = compute_euclidian_distances(mid_dict, pixelsPerMetric)
    
    return box, mid_dict, object_dims, pixelsPerMetric