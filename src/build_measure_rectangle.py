# -*- coding: utf-8 -*-
"""
Created on Tue May 23 21:46:56 2023

@author: nelso
"""

import cv2

from src.utils import get_color, get_the_needed_cut


def build_the_box(image, box, color):

    '''
    This function draws contours of each object and enhance the figure with red
    point on each coordinate.
    
    Parameters:
            image (Array): Frame used as reference to measure object(s)
            box (array) : Coordinates of the rectangle before resizing 
            color (str): color name, red|blue|green
    
    '''    
    
    cv2.drawContours(image, [box.astype("int")], -1, get_color(color), 2)
    # loop over the original points and draw them
    for (x, y) in box:
        cv2.circle(image, (int(x), int(y)), 5, (0, 0, 255), -1)

def display_mid_points(mid_dict, image):

    '''
    This function displays blue point on each middle points of the rectangle
    and connect them with a vertical and a horizontal line.
    
    Parameters:
            image (array) : Streamed image
            mid_dict (dict) : Contains intermediary point (middle point)
                              of each rectangle line (build from box input)
    '''        

    # draw the midpoints on the image
    cv2.circle(image, mid_dict["up_mid"], 5, (255, 0, 0), -1)
    cv2.circle(image, mid_dict["bottom_mid"], 5, (255, 0, 0), -1)
    cv2.circle(image, mid_dict["left_mid"], 5, (255, 0, 0), -1)
    cv2.circle(image, mid_dict["right_mid"], 5, (255, 0, 0), -1)
    # draw lines between the midpoints
    cv2.line(image, mid_dict["up_mid"], mid_dict["bottom_mid"],
    (255, 0, 255), 2)
    cv2.line(image, mid_dict["left_mid"], mid_dict["right_mid"],
    (255, 0, 255), 2)


def display_measurement(image, object_dims, mid_dict):    

    '''
    This function displays the dimensions of the object at the edge of each middle point.
    The upper measure is the height and the right measure is the width.
    
    Parameters:
            image (array) : Streamed image
            object_dims (tuple) : 2 float elements, contains dimensions of an object
                                 (width in cm, height in cm)
            mid_dict (dict) : Contains intermediary point (middle point)
                              of each rectangle line (build from box input)
    '''          


    object_dims = [f"{round(dim, 1)}cm" if type(dim) != str else ""
                         for dim in object_dims]    
    
    height_text_coor = get_the_needed_cut(mid_dict["up_mid"], (15, 10))
    width_text_coor = get_the_needed_cut(mid_dict["right_mid"], (15, 10))
    
    cv2.putText(image, object_dims[1],
    height_text_coor, cv2.FONT_HERSHEY_SIMPLEX,
    0.65, (255, 255, 255), 2)
    cv2.putText(image, object_dims[0],
    width_text_coor, cv2.FONT_HERSHEY_SIMPLEX,
    0.65, (255, 255, 255), 2)


def display_final_plot(image, box, mid_dict, object_dims, color="green"):
    
    """
    This function compiles the above functions.
    
    Parameters:
            box (array) : Coordinates of the rectangle we need to measure 
            mid_dict (dict) : Contains intermediary point (middle point)
                              of each rectangle line (build from box input)
            object_dims (tuple) : 2 float elements, contains dimensions of an object
                                 (width in cm, height in cm) 
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
    """
  
    build_the_box(image, box, color)
    display_mid_points(mid_dict, image)
    display_measurement(image, object_dims, mid_dict)