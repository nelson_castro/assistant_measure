# -*- coding: utf-8 -*-
"""
Created on Tue May 23 21:48:30 2023

@author: nelso
"""

import cv2
import imutils
from imutils import contours
import numpy as np

from src.utils import (get_the_needed_cut,
                       line_intersection)
from src.get_euclidian_distances import (get_box_mid_points,
                                         compute_needed_metrics)
from src.build_measure_rectangle import display_final_plot
from src.speech_to_text import request_cut, request_cut_size


def object_new_size(object_dim, box, wanted_size, pixelsPerMetric):

    '''
    This function uses the user requested resizing and compute the cropped-rectangle dimensions.
    
    Parameters:
            box (array) : Coordinates of the rectangle before resizing 
            object_dims (tuple) : 2 float elements, contains dimensions of an object
                                 (width in cm, height in cm)
            wanted_size (tuple) : 2 float elements, new dimensions of the target object
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
    
    Returns:
            box_copy (array) : Coordinates of the rectangle after resizing 
            wanted_size (tuple) : 2 float elements, new dimensions of the target object, it's
                                  returned because we potentially process it when one
                                  the tuple element is higher than the actuel target object dimension
                                  (from object_dims), in this case we replace the wanted size
                                  by "No cut" to avoid "0cm" to appear the final image.
    '''    

    # Detect cut asks
    my_needed_cut = get_the_needed_cut(object_dim, wanted_size)
    my_needed_pixel_cut = [round(cropped * pixelsPerMetric)
                              for cropped in my_needed_cut]
    wanted_size = tuple("No Cut" if dim_cut == 0 else dim
                     for dim, dim_cut in zip(wanted_size, my_needed_cut))
    
    box_copy = box.copy()
    
    tmp_vertical_ul = box_copy[0] + [0, my_needed_pixel_cut[1]]
    tmp_horizontal_lr = box_copy[2] + [-my_needed_pixel_cut[0], 0]
    tmp_vertical_ur = box_copy[1] + [-my_needed_pixel_cut[0], 0]
    tmp_horizontal_ur = box_copy[1] + [0, my_needed_pixel_cut[1]]
    
    box_copy[1] = line_intersection((tmp_vertical_ul, tmp_horizontal_ur),
                                    (tmp_horizontal_lr, tmp_vertical_ur))
    
    box_copy[2] = line_intersection((box_copy[1], tmp_horizontal_lr),
                                    (box_copy[3], box_copy[2]))
    
    box_copy[0] = line_intersection((tmp_vertical_ul, box_copy[1]),
                                    (box_copy[0], box_copy[3]))

    return box_copy, wanted_size

def extend_line(p1, p2, distance=10000):
    
    '''
    This function draws a line between two coordinates and extend this line
    until the end of the image.
    
    Parameters:
            p1 (array) : First coordinate
            p2 (array) : Second coordinate

    Returns:
        (tuple) : image edge coordinates to build the extended lines.
    '''
    
    diff = np.arctan2(p1[1] - p2[1], p1[0] - p2[0])
    p3_x = int(p1[0] + distance*np.cos(diff))
    p3_y = int(p1[1] + distance*np.sin(diff))
    p4_x = int(p1[0] - distance*np.cos(diff))
    p4_y = int(p1[1] - distance*np.sin(diff))
    
    return ((p3_x, p3_y), (p4_x, p4_y))


# find contours in the edge map
def extract_cam_contours(my_mask):
    
    '''
    Grab contours from an image (array), potentially multiple contours. We keep
    only the two biggest objects because we expect the images we analyzed to contain
    only two objects : the object of reference and the target object.
    With this filtering rule of keeping two biggest objects we make sure we ignore
    noise from the image.
    
    Parameters:
            my_mask (Array): Frame used as reference to measure object(s)
    
    Returns:
            (tuple) : 2 array elements, each of them contains the coordinates
            representing the contours of an object.
    '''    
    
    cnts = cv2.findContours(my_mask,
                            cv2.RETR_EXTERNAL,
                            cv2.CHAIN_APPROX_SIMPLE)
    
    cnts = sorted(imutils.grab_contours(cnts), key=lambda tup: len(tup))[-2:]    
    return contours.sort_contours(cnts)[0]
  
def object_new_size_process(orig, object_dims, box, wanted_size, pixelsPerMetric):

    '''
    This function uses the target object dimensions (computed using reference circular object)
    and will use the dimensions requested by the user to draw lines showing where the
    target object should be cut to align with these new dimensions.
    
    Parameters:
            orig (array) : Streamed image
            object_dims (tuple) : 2 float elements, contains dimensions of an object
                                 (width in cm, height in cm) 
            box (array) : Coordinates of the rectangle we need to measure 
            wanted_size (tuple) : 2 float elements, new dimensions of the target object
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)

    '''

    box_copy, wanted_size = object_new_size(object_dims, box, wanted_size, pixelsPerMetric)
    mid_dict_new = get_box_mid_points(box_copy)
    display_final_plot(orig, box_copy, mid_dict_new, wanted_size, color="red")
    
    p1, p2 = extend_line(box_copy[1], box_copy[2])
    p3, p4 = extend_line(box_copy[0], box_copy[1])
    
    cv2.line(orig, p1, p2, (0,0,255), 2)
    cv2.line(orig, p3, p4, (0,0,255), 2)   

def get_contours_metrics_live(img, masks_bw, cut_to_request, pixelsPerMetric = None):

    '''
    This function displays in streaming dimension of objects visible in a image.
    
    Parameters:
            img (array) : Streamed image
            masks_bw (array) : Frame (screenshot) of reference.
            cut_to_request (Boolean) : True if the user didn't requested a resizing yet
            or if he wants new resizing dimensions.
            pixelsPerMetric (float) : Number of pixels for 1 centimer (can be other metric system)
    '''      


    global wanted_size
    
    cnts = extract_cam_contours(masks_bw)    

    for contour in cnts: # [-2:]:
        #orig = img.copy()
        box, mid_dict, object_dims, pixelsPerMetric = compute_needed_metrics(contour,
                                                                             pixelsPerMetric)
        display_final_plot(img, box, mid_dict, object_dims)

    if cut_to_request:
        print(f"L'objet mesure {round(object_dims[0], 1)} de large "
              f"sur {round(object_dims[1], 1)} de hauteur.")
        
        to_cut = request_cut()
        if to_cut:
            wanted_size = request_cut_size()
        else:
            wanted_size = object_dims
        
    #print("Voulez-vous redimensionner cet objet ? ")
    #print("Merci d'indiquer la dimension souhaitée en largeur x hauteur.")
    object_new_size_process(img, object_dims, box, wanted_size, pixelsPerMetric)