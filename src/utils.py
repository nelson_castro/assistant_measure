# -*- coding: utf-8 -*-
"""
Created on Tue May 23 21:38:06 2023

@author: nelso
"""

import re


def get_the_needed_cut(my_size, *args):
    
    '''
    Use a tuple representing object dimension in pixel and substract it by
    the user request size (in cm also)

    Parameters:
            my_size (tuple): 2 elements tuple, (object width, object height) in cm

    Returns:
            (tuple) : substracted dimensions in cm
    '''
    
    needed_cut = tuple(map(lambda i, j: round(i - j, 3), my_size, args[0]))
    
    # MAKES SURE WE DONT WORK WITH NEGATIVE VALUES WHEN THE
    # ASKED CUT IS HIGHER THAN ACTUAL DIMENSIONS
    return tuple(0 if i < 0 else i for i in needed_cut)


def line_intersection(line1, line2):
    
    '''
    Build lines between two points and find intersection between two lines.
    
    Parameters:
            line1 (tuple): 2 elements tuple, each element contain point coordinates
            line2 (tuple): 2 elements tuple, each element contain point coordinates

    Returns:
            x (int) : intersection point x-axis coordinate
            y (int) : intersection point y-axis coordinate
    '''
    
    xdiff = (line1[0][0] - line1[1][0], line2[0][0] - line2[1][0])
    ydiff = (line1[0][1] - line1[1][1], line2[0][1] - line2[1][1])

    def det(a, b):
        return a[0] * b[1] - a[1] * b[0]

    div = det(xdiff, ydiff)
    if div == 0:
       raise Exception('lines do not intersect')

    d = (det(*line1), det(*line2))
    x = int(det(d, xdiff) / div)
    y = int(det(d, ydiff) / div)
    return x, y

def get_asked_dimensions(text_from_speech):

    '''
    Extract digits (int or float) from a string and insert it in a tuple
    
    Parameters:
            text_from_speech (str): Output of speechrecognition library

    Returns:
            (tuple) : Store all digits found in string
    '''

    my_regex = r'[\d\.\d]+'
    clean_text = text_from_speech.replace(",", ".")
    requested_dims = re.findall(my_regex, clean_text)
    return tuple((float(dim) for dim in requested_dims))

def get_cut_answer(text_from_speech):

    '''
    Search for a "oui" substring in a string, needed for future object resizing. 
    
    Parameters:
            text_from_speech (str): Output of speechrecognition library

    Returns:
            Boolean, True if "oui" in text_from_speech else False
    '''    

    clean_text = text_from_speech.lower()
    
    if "oui" in clean_text:
        print("Nous allons procéder à la paramétrisation de la découpe.")
        return True
    else:
        print("Pas de découpe demandée.")
        return False


def get_color(color):

    '''
    Extract color code.
    
    Parameters:
            color (str): color name in lower case

    Returns:
            (tuple) : Color code
    '''    

    if color == "red":
        return (0, 0, 255)
    elif color == "blue":
        return (255, 0, 0)
    else:
        return (0, 255, 0)


