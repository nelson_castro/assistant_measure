# -*- coding: utf-8 -*-
"""
Created on Tue May 23 21:39:08 2023

@author: nelso
"""

import cv2
import numpy as np


def extract_sam_mask(predictor, point_matrix, frame):

    print("Loading SAM for the screenshot")
    
    predictor.set_image(frame)

    input_point = np.array(point_matrix)
    input_label = np.array([1, 1])
    
    masks, _, _ = predictor.predict(
        point_coords=input_point,
        point_labels=input_label,
        multimask_output=False,
    )
    
    h, w = masks.shape[-2:]
    masks_bw = masks.astype(np.uint8).reshape(h, w, 1) * 255 #convert to an unsigned byte

    cv2.imwrite("in_memory_to_disk.png", masks_bw)
    return masks_bw, True