# -*- coding: utf-8 -*-
"""
Created on Sun Apr 30 11:29:28 2023

@author: nelso
"""

#import Library 
import speech_recognition as sr   

import src.constants as cst
from src.utils import (get_asked_dimensions,
                       get_cut_answer)


def request_cut():
    
    '''
    Run a speechrecognition instance to determine whether or not the object
    measured should be resized (cut) or not. It expects a "oui" in the 
    audio command to run the resizing.
    '''
    
    # this will be used to get audio from the microphone                                                                        
    v = sr.Recognizer()    
    v.energy_threshold = 50
    
    #Here, we represent our microphone as source
    msg_collected = False
    while not msg_collected:                                                                                
        
        with sr.Microphone() as source:
            v.adjust_for_ambient_noise(source,duration=1)                                                                       
            print("Bonjour, merci d'indiquer par oui ou par non la nécessité d'une découpe :") 
            #This is where it listens to our speech before going further to recognize it                                                                                  
            the_audio = v.listen(source)  
        
        try: 
            text = v.recognize_google(the_audio, language=cst.LANGUAGE)
            msg_collected = True
            return get_cut_answer(text)
        except sr.UnknownValueError: 
            print("Je n'ai pas compris, veuillez répéter.")    
    

def request_cut_size():

    '''
    Run a speechrecognition instance to determine the requested new dimensions of the
    object (after resizing).
    '''    

    # this will be used to get audio from the microphone                                                                        
    v = sr.Recognizer()    
    v.energy_threshold = 50
    
    #Here, we represent our microphone as source
    msg_collected = False
    while not msg_collected:                                                                                
        
        with sr.Microphone() as source:
            v.adjust_for_ambient_noise(source,duration=1)                                                                       
            print("Bonjour, merci d'indiquer clairement la découpe voulue (Largeur x Hauteur) :") 
            #This is where it listens to our speech before going further to recognize it                                                                                  
            the_audio = v.listen(source)  
        
        try: 
            text_dim = v.recognize_google(the_audio, language=cst.LANGUAGE)
            wanted_size = get_asked_dimensions(text_dim)
            print(f"Merci, la dimension demandée est : {wanted_size}")
            msg_collected = True
            return wanted_size
        except sr.UnknownValueError: 
            print("Je n'ai pas compris, veuillez répéter.")
       

"""
if __name__ == "__main__":
    wanted_size = oral_request_cut()
    print(wanted_size)
"""